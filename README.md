![JSX Compiler](/JSX%20Compiler.gif)

# JSX Compiler #
JSX Compiler is a utility script to compile individual or deeply nested JSX files to JSXBIN from After Effects.

**NOTE:** You must have ExtendScript Toolkit installed for script to work.

### Installation: ###
Copy **JSX Compiler.jsx** to ScriptUI Panels folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking Window -> **JSX Compiler**

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------