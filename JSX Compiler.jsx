/**********************************************************************************************
    Compile JSX to JSXBIN files from After Effects.
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
    	Execute this script from After Effects.
    	Compile individual or deeply nested JSX files to JSXBIN from After Effects.
    	Choose to keep or remove original JSX files after compilation is finished.
    	NOTE: You must have ExtendScript Toolkit installed for script to work.

	Change log:
		v1.1 - 2017 11 17
			- Adds predefined look-up paths for ESTK application

		v1.0 - 2017 09 08
			- Initial release

	Released as open-source under the MIT license:
		The MIT License (MIT)
		Copyright (c) 2017 Tomas Šinkūnas www.renderTom.com
		Permission is hereby granted, free of charge, to any person obtaining a copy of this
		software and associated documentation files (the "Software"), to deal in the Software
		without restriction, including without limitation the rights to use, copy, modify, merge,
		publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
		to whom the Software is furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all copies or
		substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
		INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
		PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
		FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
		OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
		DEALINGS IN THE SOFTWARE.

**********************************************************************************************/

(function (thisObj) {
	var scriptName = "JSX Compiler",
		compilerOptions = {
			removeOriginalFile: false,
			selectScriptFolder: false
		},
		isWin = $.os.indexOf("Windows") > -1;

	var jsxCompilerESTK = function () {
		var compilerOptions, scriptFiles, isWin, PB, numCompiledFiles = 0;

		compilerOptions = {
			removeOriginalFile: false,
			selectScriptFolder: false
		};

		isWin = $.os.indexOf("Windows") > -1;
		PB = new ProgressBar("JSX compiler");

		scriptFiles = getScriptFiles(compilerOptions.selectScriptFolder);
		if (!scriptFiles) return;

		PB.show("Compiling files.", scriptFiles.length);
		for (var i = 0, il = scriptFiles.length; i < il; i++) {
			PB.up(File.decode(scriptFiles[i].displayName) + " (" + (i + 1) + "/" + scriptFiles.length + ")");
			if (exportJSBIN(scriptFiles[i])) {
				numCompiledFiles++;
				if (compilerOptions.removeOriginalFile === true) {
					scriptFiles[i].remove();
				}
			}
		}

		PB.close();
		alert("Compiled " + numCompiledFiles + " " + ((numCompiledFiles > 1) ? "scripts." : "script."));

		// Utilities
		// ---------------
		function exportJSBIN(inputFile) {
			var fileContent = readFileContent(inputFile),
				outputFile = changeExtension(inputFile, "jsxbin"),
				compiledString,
				includepath,
				compileSuccess = true;

			try {
				includepath = inputFile.parent ? inputFile.parent.absoluteURI : "/";
				compiledString = app.compile(fileContent, inputFile.absoluteURI, includepath);
				writeFile(outputFile, compiledString);
			} catch (e) {
				compileSuccess = false;
				alert("Unable to compile file " + File.decode(inputFile.displayName) + "\nFile path: " + inputFile.fsName + "\n" + e.toString() + "\nLine: " + e.line.toString());
			}

			return compileSuccess;
		}

		function getScriptFiles(getScriptsInFolders) {
			var scriptFiles, targetFolder;

			if (getScriptsInFolders === true) {
				targetFolder = Folder.selectDialog("Select scripts folder you want to compile");
				if (targetFolder) {
					scriptFiles = getAllFiles(targetFolder, true, "js, jsx");
				}
			} else {
				scriptFiles = selectFiles(true, "js, jsx", "Please select \"*.js\" or \"*.jsx\" files");
			}

			return scriptFiles;
		}

		// Read Write
		// ---------------
		function readFileContent(fileObj, encoding) {
			var fileContent;
			encoding = encoding || "utf-8";

			fileObj.open('r');
			fileObj.encoding = encoding;
			fileContent = fileObj.read();
			fileObj.close();
			return fileContent;
		}

		function writeFile(pathToFile, fileContent, encoding) {
			var newFile = (pathToFile instanceof File) ? pathToFile : new File(pathToFile);
			encoding = encoding || "utf-8";

			newFile.encoding = encoding;
			newFile.open("w");
			newFile.write(fileContent);
			newFile.close();
		}

		// Files
		// ---------------
		function getAllFiles(pathToFolder, recursion, extensionList) {
			var pathFiles = Folder(pathToFolder).getFiles(),
				files = [],
				subfiles = [];

			if (extensionList === undefined)
				extensionList = "";
			else if (typeof extensionList !== "function")
				extensionList = new RegExp("\.\(" + extensionList.replace(/,/g, "|").replace(/ /g, "") + ")$", "i");

			PB.show("Collecting files.", pathFiles.length);
			for (var i = 0, il = pathFiles.length; i < il; i++) {
				if (pathFiles[i] instanceof File) {
					PB.up(File.decode(pathFiles[i].displayName));
					if (pathFiles[i].name.match(extensionList)) {
						files.push(pathFiles[i]);
					}
				} else if (pathFiles[i] instanceof Folder && recursion === true) {
					subfiles = getAllFiles(pathFiles[i], recursion, extensionList);
					for (var j = 0, jl = subfiles.length; j < jl; j++) {
						files.push(subfiles[j]);
					}
				}
			}
			return files;
		}

		function selectFiles(multiSelect, extensionList, infoMessage) {
			infoMessage = infoMessage || (multiSelect ? "Please select multiple files" : "Please select file");
			var filter = isWin ? "*." + extensionList.replace(/ /g, "").replace(/,/g, ";*.") : function (file) {
				var re = new RegExp("\.\(" + extensionList.replace(/,/g, "|").replace(/ /g, "") + ")$", "i");
				if (file.name.match(re) || file.constructor.name === "Folder")
					return true;
			};
			return File.openDialog(infoMessage, filter, multiSelect);
		}

		function changeExtension(file, newExtension) {
			file = (file instanceof File) ? file : new File(file);
			return file.fsName.substring(0, file.fsName.lastIndexOf(".")) + "." + newExtension;
		}

		function ProgressBar(panelTitle) {
			"use strict";

			var win = new Window("palette", panelTitle, undefined);
			win.alignChildren = ["fill", "top"];
			win.preferredSize.width = 360;
			win.spacing = 10;

			win.etProgressName = win.add("statictext");
			win.pbProgressBar = win.add("progressbar");
			win.pbProgressBar.preferredSize.height = 5;
			win.etProgressInfo = win.add("statictext");

			this.name = function (progressName) {
				win.etProgressName.text = progressName;
			};

			this.info = function (progressInfo) {
				win.etProgressInfo.text = progressInfo;
			};

			this.show = this.reset = function (progressName, maxValue) {
				win.pbProgressBar.value = 0;
				win.pbProgressBar.maxvalue = maxValue;

				this.name(progressName);

				win.center();
				win.show();
			};

			this.up = function (progressInfo) {
				win.pbProgressBar.value++;

				this.info(progressInfo);
				win.update();
			};

			this.hide = function () {
				win.hide();
			};

			this.close = function () {
				win.close();
			};
		}
	};

	scriptBuildUI(thisObj);

	function main() {
		var pathToESTK, compilerScript, cmd;

		if (!canWriteFiles()) return;

		pathToESTK = getPathToESTK();
		if (!pathToESTK) return;

		compilerScript = getCompilerScript();
		if (!compilerScript) return;

		cmd = "cd " + "\"" + compilerScript.parent.fsName + "\"";
		if (isWin) {
			cmd = "cmd /c " + cmd + " && \"" + pathToESTK + "\" -cmd \"" + compilerScript.displayName + "\"";
		} else {
			cmd = cmd + "; " + "\"" + pathToESTK + "\"" + " -cmd " + "\"" + compilerScript.name + "\"";
		}

		system.callSystem(cmd);
		// compilerScript.remove();
	}

	function scriptBuildUI(thisObj) {
		var win = (thisObj instanceof Panel) ? thisObj : new Window("palette", scriptName, undefined, {
			resizeable: true
		});
		win.alignChildren = ["fill", "top"];
		win.spacing = 2;

		win.checkDeleteOriginal = win.add("checkbox", undefined, "Delete original files");
		win.checkDeleteOriginal.helpTip = "Removes original JSX file after compilation.";
		win.checkDeleteOriginal.onClick = function () {
			compilerOptions.removeOriginalFile = this.value;
		};

		win.btnProcessFolder = win.add("button", undefined, "Process folder");
		win.btnProcessFolder.helpTip = "Precess deeply nested JSX files in folders";
		win.btnProcessFolder.onClick = function () {
			compilerOptions.selectScriptFolder = true;
			main();
		};

		win.btnSelectIndividual = win.add("button", undefined, "Process individual files");
		win.btnSelectIndividual.helpTip = "Select individual JSX files to compile.";
		win.btnSelectIndividual.onClick = function () {
			compilerOptions.selectScriptFolder = false;
			main();
		};

		win.onResizing = win.onResize = function () {
			this.layout.resize();
		};

		if (win instanceof Window) {
			win.center();
			win.show();
		} else {
			win.layout.layout(true);
			win.layout.resize();
		}
	}

	function getCompilerScript() {
		var compilerAsString, compilerFile;

		compilerAsString = getCompilerAsString();
		compilerAsString = compilerAsString.replace("removeOriginalFile: false", "removeOriginalFile: " + compilerOptions.removeOriginalFile);
		compilerAsString = compilerAsString.replace("selectScriptFolder: false", "selectScriptFolder: " + compilerOptions.selectScriptFolder);

		compilerFile = new File("~/Desktop/JSX Compiler-ESTK.js");
		writeFile(compilerFile, compilerAsString);

		return compilerFile;
	}

	function getCompilerAsString() {
		var disclaimerString =
			"/**********************************************************************************************\n" +
			"\tJSX Compiler-ESTK.js\n" +
			"\tCopyright (c) 2017 Tomas Šinkūnas. All rights reserved.\n" +
			"\twww.rendertom.com\n" +
			"\n" +
			"\tDescription:\n" +
			"\t\tExecute this script from ExtendScript Toolkit.\n" +
			"\t\tChange \"compilerOptions\" to your liking.\n" +
			"\n" +
			"\tReleased as open-source under the MIT license:\n" +
			"\t\tThe MIT License (MIT)\n" +
			"\t\tCopyright (c) 2017 Tomas Šinkūnas www.renderTom.com\n" +
			"\t\tPermission is hereby granted, free of charge, to any person obtaining a copy of this\n" +
			"\t\tsoftware and associated documentation files (the \"Software\"), to deal in the Software\n" +
			"\t\twithout restriction, including without limitation the rights to use, copy, modify, merge,\n" +
			"\t\tpublish, distribute, sublicense, and/or sell copies of the Software, and to permit persons\n" +
			"\t\tto whom the Software is furnished to do so, subject to the following conditions:\n" +
			"\n" +
			"\t\tThe above copyright notice and this permission notice shall be included in all copies or\n" +
			"\t\tsubstantial portions of the Software.\n" +
			"\n" +
			"\t\tTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,\n" +
			"\t\tINCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR\n" +
			"\t\tPURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE\n" +
			"\t\tFOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR\n" +
			"\t\tOTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER\n" +
			"\t\tDEALINGS IN THE SOFTWARE.\n" +
			"\n" +
			"**********************************************************************************************/\n\n";

		var compilerAsString = jsxCompilerESTK.toString();
		return disclaimerString + "//@target estoolkit#dbg\n(" + compilerAsString + ")();";
	}

	function getPathToESTK() {
		try {
			var pathToESTK = "",
				pathToExec = "",
				searchPath = "",
				extension = "",
				executable = null,
				pathsToApp;

			if (isWin) {
				pathsToApp = [
					"C:\\Program Files (x86)\\Adobe\\Adobe ExtendScript Toolkit CC\\ExtendScript Toolkit.exe"
				];
				extension = "exe";
			} else {
				pathsToApp = [
					"/Applications/Adobe ExtendScript Toolkit CC/ExtendScript Toolkit.app",
					"/Applications/!_Adobe/Adobe ExtendScript Toolkit CC/ExtendScript Toolkit.app"
				];
				pathToExec = "/Contents/MacOS/ExtendScript Toolkit";
				extension = "app";
			}

			executable = getExecutable(pathsToApp, pathToExec);

			if (executable === null) {
				pathToESTK = selectFiles(false, extension, "Please select ExtendScript Toolkit." + extension);
				if (pathToESTK) {
					searchPath = pathToESTK.fsName + pathToExec;
					if (File(searchPath).exists) {
						executable = searchPath;
					} else {
						alert("\"" + File.decode(pathToESTK.name) + "\" is not ExtendScript " + File.decode("%C2%AF%5C_(%E3%83%84)_/%C2%AF") + ". Try again!");
					}
				}
			}

			return executable;


		} catch (e) {
			alert(e.toString() + "\nLine: " + e.line.toString());
		}

	}

	function getExecutable(pathsToApp, pathToExec) {
		var executable, i, il;

		for (i = 0, il = pathsToApp.length; i < il; i++) {
			executable = pathsToApp[i] + pathToExec;
			if (File(executable).exists) {
				return executable;
			}
		}
		return null;
	}

	function writeFile(pathToFile, fileContent, encoding) {
		var newFile = (pathToFile instanceof File) ? pathToFile : new File(pathToFile);
		encoding = encoding || "utf-8";

		newFile.encoding = encoding;
		newFile.open("w");
		newFile.write(fileContent);
		newFile.close();
	}

	function canWriteFiles() {
		if (!isSecurityPrefSet()) {
			alert(scriptName + " requires access to write files.\n" +
				"Go to the \"General\" panel of the application preferences and make sure " +
				"\"Allow Scripts to Write Files and Access Network\" is checked.");
			app.executeCommand(2359);
			if (!isSecurityPrefSet()) return null;
		}
		return true;
	}

	function isSecurityPrefSet() {
		var securitySetting = app.preferences.getPrefAsLong("Main Pref Section", "Pref_SCRIPTING_FILE_NETWORK_SECURITY");
		return (securitySetting === 1);
	}

	function selectFiles(multiSelect, extensionList, infoMessage) {
		infoMessage = infoMessage || (multiSelect ? "Please select multiple files" : "Please select file");
		var filter = ($.os.indexOf("Windows") != -1) ? "*." + extensionList.replace(/ /g, "").replace(/,/g, ";*.") : function (file) {
			var re = new RegExp("\.\(" + extensionList.replace(/,/g, "|").replace(/ /g, "") + ")$", "i");
			if (file.name.match(re) || file.constructor.name === "Folder")
				return true;
		};
		return File.openDialog(infoMessage, filter, multiSelect);
	}

})(this);